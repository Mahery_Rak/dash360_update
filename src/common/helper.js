import React, {Component} from 'react';

export const numberWithCommas = value => {
    return  value.toString().replace( /\B(?=(\d{3})+(?!\d))/g, ',')
}

export class HighLightWords extends Component {

    createMarkup = () => {
        let {className, text, words} = this.props;

        words = words.split(' ');
        let ignoreList = ["le", "la", "les", "l'", "à", "aux", "de", "du", "des", "ce", "ces", "un", "une", "que", "qui"]
        for(let i=0; i<words.length; i++) {
            if (!ignoreList.includes(words[i]) && words[i].length > 1) {
                let regex = new RegExp(words[i], "gi");
                text = text.replace(regex, '<span class="' +className+ '">' +words[i]+ '</span>')
            }
        }
        return {
            __html: text
        }
    }

    render () {
        return (
            <div style={this.props.style} dangerouslySetInnerHTML={this.createMarkup()} />
        )
    }
}