export const ERROR_STATUS_PAGES =           [-1, 404, 500];
export const ERROR_STATUS_FOR_REDIRECT =    [-1, 500];
export const SERVICE_BASE_URL =             "http://" +window.location.hostname+ ":8000";
export const AUTH_BASE_URL =                "http://" +window.location.hostname+ ":8001";
export const CRAWL_BASE_URL =               "http://" +window.location.hostname+ ":8002";
export const MOVE_THRESHOLD =               20;