import axios from 'axios'

export let APIAxios = axios.create({
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('user')
    }
})