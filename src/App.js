import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {Switch, Route, Redirect} from 'react-router-dom';

import classNames from 'classnames';

import { ERROR_STATUS_PAGES, ERROR_STATUS_FOR_REDIRECT } from './common';

import Sidebar from './containers/sideBar';
import Header from './containers/header';

import {checkAuthentication} from './actions/authenticationAction';

import {Dashboard, LoginPage, AppError, Settings } from './pages';

import  './static/css/App.css';

const mapStateToProps = (state) => {
  return {
      logged: state.auth.logged,
      serverAccessState: state.serverAccess,
      isOpen: state.sideBarState.isOpen
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({checkAuthentication}, dispatch);

class App extends Component {


  componentWillMount() {
    const {checkAuthentication} = this.props;
    checkAuthentication();
  }

  componentDidUpdate() {
    const {logged, location, history, serverAccessState} = this.props;
    let path = location.pathname;
    
    if(!path.includes("/error")){
      if((ERROR_STATUS_FOR_REDIRECT.includes(serverAccessState))){
          history.push('/error/-1');
      }else{
          if ((!logged) && (!!!path.includes("/login"))){
              history.push('/login');
          }
          if (logged && (path.includes("/login"))){
              history.push('/');
          }
      }
    }
  }

  render() {
    const {isOpen} = this.props;

    const pageContentClass = classNames({
      'page-content': true,
      'menu-open': isOpen
    });

    return (
      <div>
        <Header />
        <Sidebar />
        <div className={pageContentClass} style={{marginLeft:'90px'}}>          
          <Switch>
            <Route exact path={'/error/:errorStatus('+ ERROR_STATUS_PAGES.join('|') +')'} component={AppError}/>
            <Route exact path="/" component={Dashboard} />
            <Route exact path="/settings" component={Settings} />
            <Route path="/login" component={LoginPage} />
            <Redirect from="*" to="error/404"/>
          </Switch>
        </div>
      </div>
    )
  }
}

App = connect(mapStateToProps, mapDispatchToProps)(App);
App = withRouter(App);
export default App;