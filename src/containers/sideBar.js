import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import PropTypes from 'prop-types';

import classNames from 'classnames';

import {NavLink} from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider'
import DashboardIcon from '@material-ui/icons/Dashboard'
import SettingsIcon from '@material-ui/icons/Settings'
import DoneAllIcon from '@material-ui/icons/DoneAll'

import {toggleSideBarAction} from '../actions'

const drawerWidth = 240;

const mapStateToProps = (state) => {
  return {
    isOpen: state.sideBarState.isOpen
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ toggleSideBarAction }, dispatch);

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: 800,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
});

class Sidebar extends React.Component {

  handleDrawerClose = () => {
    const {toggleSideBarAction} = this.props;
    toggleSideBarAction();
  };

  render() {
    const { classes, theme, isOpen } = this.props;
    
    return (
      
      <Drawer
        variant="permanent"
        classes={{
          paper: classNames(classes.drawerPaper, !isOpen && classes.drawerPaperClose),
        }}
        open={isOpen}
        style={{postion: 'absolute', left: 0, top: '50px', height: '100%'}}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={this.handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List component="nav">
            <NavLink to="/" exact activeClassName="active">
              <ListItem>
                <ListItemIcon>
                  <DashboardIcon />
                </ListItemIcon>
                  <ListItemText primary="Dashboard"/>
              </ListItem>
            </NavLink>

            <NavLink to="/settings" exact activeClassName="active">
              <ListItem>
                <ListItemIcon>
                  <SettingsIcon />
                </ListItemIcon>
                <ListItemText primary="Settings" />
              </ListItem>
            </NavLink>
          
            <NavLink to="/e-reputation" exact activeClassName="active">
              <ListItem>
                <ListItemIcon>
                  <DoneAllIcon />
                </ListItemIcon>
                <ListItemText primary="E-Reputation" />
              </ListItem>
            </NavLink>
        </List>
      </Drawer>
    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

Sidebar = connect(mapStateToProps, mapDispatchToProps)(Sidebar)

export default withStyles(styles, { withTheme: true })(Sidebar);
