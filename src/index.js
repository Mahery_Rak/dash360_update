import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';


import registerServiceWorker from './registerServiceWorker';

import store from './store';

import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

import App from './App';


const theme = createMuiTheme ({
    palette: {
        primary1Color: '#2DD1AC',
        accent1Color:  '#2DD1AC',
    }
})


ReactDOM.render( 
<Provider store={store}>
    <MuiThemeProvider theme = {theme}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </MuiThemeProvider>
</Provider>
,document.getElementById('root'));
registerServiceWorker();
