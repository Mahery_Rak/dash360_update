import {combineReducers} from 'redux';

import sideBarState from './sideBarStateReducers';
import auth from './authenticationReducers';
import serverAccess from './serverAccessReducers';
import settings from './settingsReducer';


const allReducers = combineReducers({
    sideBarState,
    auth,
    serverAccess,
    settings,
});

export default allReducers;