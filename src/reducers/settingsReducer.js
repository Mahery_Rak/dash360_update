import update from 'react-addons-update';

const defaultState = () => {
    return {
        facebookCronJob: {
            keywords: [],
            keywordsStat: [],
            isLoading: true
        },
        articleCronJob: {
            articles: [],
            articleJobsStat: [],
            isLoading: true
        }
    }
}
export default function(state=defaultState(), action) {
    switch(action.type) {
        case 'SETTINGS_GET_ALL_KEYWORDS_START':
            return update(state, {
                facebookCronJob: {
                    keywords: {$set: []},
                    isLoading: {$set: true}
                }
            })
        case 'SETTINGS_GET_ALL_KEYWORDS_SUCCESS':
            const keywords = action.keywords;
            return update(state, {
                facebookCronJob: {
                    keywords: {$set: keywords},
                    originKeywords: {$set: JSON.parse(JSON.stringify(keywords))},
                    isLoading: {$set: false}
                }
            })
        case 'SETTINGS_ADD_NEW_KEYWORD':
            return update(state, {
                facebookCronJob: {
                    keywords: {
                        $push: [{name: "", isActive:true, type: "facebook"}]
                    }
                }
            })
        case 'SETTINGS_UPDATE_KEYWORD':
            return update(state, {
                facebookCronJob: {
                    keywords: {
                        [action.index]: {
                            $set: action.keyword
                        }
                    }
                }
            })
        case 'SETTINGS_SAVE_KEYWORDS_START':
            return update(state, {
                facebookCronJob: {
                    isLoading: {$set: true}
                }
            })
        case 'SETTINGS_SAVE_KEYWORDS_SUCCESS':
            return update(state, {
                facebookCronJob: {
                    isLoading: {$set: false}
                }
            })
            
        case 'SETTINGS_GET_KEYWORDS_STAT_START':
            return update(state, {
                facebookCronJob: {
                    keywordsStat: {$set: []},
                    isLoading: {$set: true}
                }
            })
        case 'SETTINGS_GET_KEYWORDS_STAT_SUCCESS':
            return update(state, {
                facebookCronJob :{
                    keywordsStat: {$set: action.keywordsStat},
                    isLoading: {$set: false}
                }
            })


        case 'SETTINGS_GET_ALL_ARTICLE_JOBS_START':
            return update(state, {
                articleCronJob: {
                    articles: {$set: []},
                    isLoading: {$set: true}
                }
            })
        case 'SETTINGS_GET_ALL_ARTICLE_JOBS_SUCCESS':
            return update(state, {
                articleCronJob: {
                    articles: {$set: action.articles},
                    originArticleJobs: {$set: JSON.parse(JSON.stringify(action.articles))},
                    isLoading: {$set: false}
                }
            })
        case 'SETTINGS_UPDATE_ARTICLE_JOB':
            return update(state, {
                articleCronJob: {
                    articles: {
                        [action.index]: {
                            $set: action.article
                        }
                    }
                }
            })
        case 'SETTINGS_SAVE_ARTICLE_JOBS_START':
            return update(state, {
                articleCronJob: {
                    isLoading: {$set: true}
                }
            })
        case 'SETTINGS_SAVE_ARTICLE_JOBS_SUCCESS':
            return update(state, {
                articleCronJob: {
                    isLoading: {$set: false}
                }
            })
        case 'SETTINGS_GET_ARTICLES_STAT_START':
            return update(state, {
                articleCronJob: {
                    articleJobsStat: {$set: []},
                    isLoading: {$set: true}
                }
            })
        case 'SETTINGS_GET_ARTICLES_STAT_SUCCESS':
            return update(state, {
                articleCronJob :{
                    articleJobsStat: {$set: action.articleJobsStat},
                    isLoading: {$set: false}
                }
            })

        default:
            return state;
    }
}