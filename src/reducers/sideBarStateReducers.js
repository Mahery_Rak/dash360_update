const defaultState = () => {
    if (localStorage.getItem('isSidebarOpen') == null) {
        localStorage.setItem('isSidebarOpen', false)
    }
    return {
        isOpen: JSON.parse(localStorage.getItem('isSidebarOpen'))
    }
}

export default function(state=defaultState(), action) {
    switch(action.type) {
        case 'TOGGLE_SIDEBAR' :
            localStorage.setItem('isSidebarOpen', !state.isOpen);
            return {
                ...state,
                isOpen: !state.isOpen
            }
        default:
            return state;
    }
}