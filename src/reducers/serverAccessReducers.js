const serverAccess = (state = {status: null, message:null }, action) => {
    switch (action.type) {
        case 'RAISE_SERVER_ACCESS_SUCCESS':
            return {
                status: action.status
            };
        case 'RAISE_SERVER_ACCESS_FORBIDDEN':
            return {
                status: action.status
            };

        case 'RAISE_SERVER_ACCESS_UNAUTHORIZED':
            return {
                status: action.status
            };

        case 'RAISE_SERVER_ACCESS_INTERNAL_ERROR':
            return {
                status: action.status
            };
        case 'RAISE_SERVER_ACCESS_UNREACHABLE':
            return {
                status: action.status
            };                    
        default:
            return state
        }
}
export default serverAccess;