const authState = () => {
    return {
        logged: null,
        error: false
    }
}
export default function (state=authState(), action) {
    switch(action.type) {
        case 'FETCH_AUTH_SUCCESS':
            return {
                logged: true,
                error: null //token
            };
        case 'FETCH_AUTH_REJECTED':
            return {
                logged: false
            }; 
        case 'LOG_IN_AUTH_SUCCESS':
            return {
                logged: true,
                error: null
            };
        case 'LOG_IN_AUTH_REJECTED':
            return {
                logged: false,
                error: action.payload,
            };
        case 'LOG_OUT_AUTH_SUCCESS':
            return {
                logged: false
            };                     
        default:
            return state
    }
}