import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {Tabs, Tab, Icon, Typography} from '@material-ui/core';

import {KeywordsCronJob, CronJobStat, ArticlesCronJob} from '../components';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

// function Settings (props) {
//   const { classes } = props;
  class Settings extends Component {

    state = {
      value: 0,
    };
  
    handleChange = (event, value) => {
      this.setState({ value });
    };
  

  render () {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
            <Grid item xs={5}>
              <Paper className={classes.paper}>
                <b>CRON JOB POUR FACEBOOK </b>
                <Tabs value={value}  onChange={this.handleChange}>
                  <Tab
                    icon={<Icon>list</Icon>}
                    label="Keywords"
                  />
                  <Tab
                    icon={<Icon>storage</Icon>}
                    label="Volumétrie des données"
                  />
                </Tabs>
                {value === 0 && <TabContainer> <KeywordsCronJob /> </TabContainer>}
                {value === 1 && <TabContainer> <CronJobStat type="facebook" /> </TabContainer>}
              </Paper>
            </Grid>
            <Grid item xs={5}>
              <Paper className={classes.paper}>
                <b>CRON JOB POUR ARTICLE </b>
                <Tabs value={value}  onChange={this.handleChange}>
                  <Tab
                    icon={<Icon>list</Icon>}
                    label="Articles"
                  />
                  <Tab
                    icon={<Icon>storage</Icon>}
                    label="Volumétrie des données"
                  />
                </Tabs>
                {value === 2 && <TabContainer> <ArticlesCronJob /> </TabContainer>}
                {value === 3 && <TabContainer> <CronJobStat type="article" /> </TabContainer>}
              </Paper>
            </Grid>

          
        </Grid>
      </div>
    );
  }
}

Settings.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Settings);
	