import React, { Component } from 'react';

import Button from '@material-ui/core/Button';

class AppError extends Component {

    render () {
        return (
            <div className="login-wrapper" style={{background: '#1D72EC'}}>
                <div className="login-fields text-center">
                    <h3 className="title-404">{this.props.match.params.errorStatus}</h3>
                <div className="pt20">
                    <p>{this.props.match.params.errorStatus == 404 && "La page que vous recherchez n'est pas disponible..."}</p>
                </div>
                <div className="pt20">
                    <Button 
                        href="/login"
                        color="primary"
                        fullWidth={true}
                    > 
                        Retour 
                    </Button>
                </div>
            
                </div>
            </div>
        )
    }
}

export default AppError;