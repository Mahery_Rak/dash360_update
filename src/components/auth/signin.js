import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import LockIcon from '@material-ui/icons/Lock';
import Button from '@material-ui/core/Button';

import {signInAction} from '../../actions/authenticationAction';

import Alert from '../alert';

const mapStateToProps = (state) => {
    return {
        logged: state.auth.logged,
        error: state.auth.error
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ signInAction }, dispatch);

class Signin extends Component {

    constructor() {
        super();
        this.state = {
            username: "",
            password: ""
        }
    }

    handleUsernameChange = (e) => {
        this.setState({
            username: e.target.value,
        });
    }

    handlePasswordChange = (e) => {
        this.setState({
            password: e.target.value,
        });
    }

    handleKeyPress = (e) => {
        if(e.key === 'Enter' || e.charCode === 13  || e.keyCode === 13) {
            this.handleSubmit();
        }
    }

    handleSubmit = () => {
        const {signInAction, history} = this.props;
        signInAction(this.state.username, this.state.password, history);
    }

    render() {

        const {error} = this.props;

        return (
            <div className="login-wrapper" style={{background: '#1D72EC'}}>
                <form className="login-fields">
                    <h3 style={{textAlign: 'center'}}>Login</h3>
                    {error && <Alert type="danger">{error.message}</Alert>}
                    <Grid container spacing={40} alignItems="flex-end">
                        <Grid item>
                            <AccountCircle />
                        </Grid>
                        <Grid item>
                            <TextField  
                                id="username"
                                label="Username" 
                                onChange={this.handleUsernameChange}
                                onKeyPress={e => this.handleKeyPress(e)}
                            />
                        </Grid>
                    </Grid>
                    <Grid container spacing={40} alignItems="flex-end">
                        <Grid item>
                            <LockIcon />
                        </Grid>
                        <Grid item>
                            <TextField  
                                id="password"
                                label="Password"
                                type="password"
                                onChange={this.handlePasswordChange}
                                onKeyPress={e => this.handleKeyPress(e)} 
                            />
                        </Grid>
                    </Grid><br /><br />
                    
                        <Button 
                            variant="extendedFab" 
                            color="primary" 
                            fullWidth={true}
                            onClick={this.handleSubmit}
                            disabled={!this.state.username.length || !this.state.password.length} 
                        >
                            Login
                        </Button>
                    
                </form>
            </div>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Signin);