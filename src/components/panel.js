
import React from 'react';
import {Scrollbars} from 'react-custom-scrollbars';

const Panel = (props) => {

    const {rightButtons, height, header} = props;

    return(
        <div className="readmin-panel">
            <div className="panel-heading" style={{'padding': rightButtons?'5px 20px': '16px 20px'}}>
                <h5>{props.title}</h5>
                {rightButtons}
            </div>
            {header}
            {props.withScrollbar &&
            <Scrollbars style={{height: height ? height: 620}}>
                <div className="panel-body">
                    {props.children}
                </div>
            </Scrollbars>}

            {!props.withScrollbar &&
                <div className="panel-body">
                    {props.children}
                </div>
            }

            {props.footer && 
                <div className="panel-footer">
                    {props.footer}
                </div>}

        </div>
    )
}

export default Panel;