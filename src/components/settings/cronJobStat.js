import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getKeywordsCronJobStat, getArticlesCronJobStat} from '../../actions';

import MonthPickerInput from 'react-month-picker-input';

import {Button, MenuItem, Icon} from '@material-ui/core';

import {numberWithCommas} from '../../common';

const hoursList = [];
for (let i = 0; i < 24; i++ ) {
    hoursList.push(<MenuItem value={i} key={i} primaryText={`${i}h`} />);
}
const daysList = [];
for (let i = 1; i <= 10; i++ ) {
    daysList.push(<MenuItem value={i} key={i} primaryText={`${i}j`} />);
}

const mapStateToProps = (state, ownProps) => {
    let cronJobStat = []
    let isLoading = false
    if (ownProps.type == 'facebook') {
        cronJobStat = state.settings.facebookCronJob.keywordsStat
        isLoading = state.settings.facebookCronJob.isLoading
    }
    if (ownProps.type == 'article') {
        cronJobStat = state.settings.articleCronJob.articleJobsStat
        isLoading = state.settings.articleCronJob.isLoading
    }
    return {
        cronJobStat,
        isLoading
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ getKeywordsCronJobStat, getArticlesCronJobStat }, dispatch);

class KeywordsCronJobStat extends Component {

    constructor() {
        super();
        this.state = {
            filterParameters: {
                start_month: false,  
                end_month: false
            }
        }
        this.selectStartMonth = this.selectStartMonth.bind(this);
        this.selectEndMonth = this.selectEndMonth.bind(this);
        this.getStat = this.getStat.bind(this);
    }

    selectStartMonth = (selectedYear, selectedMonth) => {
        let params = this.state.filterParameters
        params.start_month = selectedYear
        this.setState({
            filterParameters: params
        });
    }
    selectEndMonth = (selectedYear, selectedMonth) => {
        let params = this.state.filterParameters
        params.end_month = selectedYear
        this.setState({
            filterParameters: params
        });
    }

    getStat = () => {
        const {type, getKeywordsCronJobStat, getArticlesCronJobStat} = this.props
        if (type == 'facebook') {
            getKeywordsCronJobStat(this.state.filterParameters)
        }
        if (type == 'article') {
            getArticlesCronJobStat(this.state.filterParameters)
        }
    }

    render() {
        let {cronJobStat, isLoading} = this.props;
        let Sommes = []

        if (cronJobStat && cronJobStat.length) {
            cronJobStat.forEach( (row, i) => {
                row.stat.forEach( (stat, j) => {
                    if(i == 0) {
                        Sommes.push(0)
                    }
                    Sommes[j] += stat.value
                })
            })
        }
        return (
            <div>
                { isLoading &&
                    <div className="full-loading">
                        <i className="fa fa-spinner fa-pulse fa-3x"></i>
                    </div>
                }
                <div className="row custom-month-picker">
                    <div className="col-md-4">Date début
                        <MonthPickerInput lang='fr' closeOnSelect={true} onChange={(selectedYear, selectedMonth) => this.selectStartMonth(selectedYear, selectedMonth)}/>
                    </div>
                    <div className="col-md-4">Date fin
                        <MonthPickerInput lang='fr' closeOnSelect={true} onChange={(selectedYear, selectedMonth) => this.selectEndMonth(selectedYear, selectedMonth)}/>
                    </div>
                    <div className="col-md-4"><span style={{'color': 'white', 'userSelect': 'none'}}>Search</span>
                        <Button
                            variant="contained"
                            fullWidth={true} 
                            icon={<Icon>search</Icon>}
                        >
                            Search
                        </Button>
                    </div>
                </div>
                {cronJobStat && cronJobStat.length > 0 &&
                    <table className="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Keyword</th>
                                {cronJobStat[0].stat.map( (stat, index) => (
                                    <th key={'head_' +index}>{stat.label}</th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                        {cronJobStat.map( (row, index) => (
                            <tr key={'row_'+index}>
                                <td>{row.name}</td>
                                {row.stat.map( (stat, index) => (
                                    <td key={'value_' +index} style={{textAlign: 'center'}}>{numberWithCommas(stat.value)}</td>
                                ))}
                                
                            </tr>
                        ))}
                        <tr>
                            <th>Total par mois</th>
                            {    
                                Sommes.map((somme, index)=> (
                                    <th key={'somme_' +index} style={{textAlign: 'center'}}>{numberWithCommas(somme)} byte(s)</th>
                                ))
                            }
                        </tr>
                        </tbody>
                    </table>                
                }
            </div>
            
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KeywordsCronJobStat);