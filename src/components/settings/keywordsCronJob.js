import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import { MenuItem,Button,Select,TextField} from '@material-ui/core';
import ToggleButton from '@material-ui/lab/ToggleButton';


import {getAllKeywords, addNewKeyword, updateKeyword, saveKeywords} from '../../actions';

import Alert from '../alert';


const hoursList = [];
for (let i = 0; i < 24; i++ ) {
    hoursList.push(<MenuItem value={i} key={i} primaryText={`${i}h`} />);
}
const daysList = [];
for (let i = 1; i <= 10; i++ ) {
    daysList.push(<MenuItem value={i} key={i} primaryText={`${i}j`} />);
}


const mapStateToProps = (state) => {
    return {
        keywords: state.settings.facebookCronJob.keywords,
        isLoading: state.settings.facebookCronJob.isLoading
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
    { getAllKeywords, addNewKeyword, updateKeyword, saveKeywords }, dispatch);


class KeywordsCronJob extends Component {

    componentWillMount() {
        const {getAllKeywords} = this.props;
        getAllKeywords();
    }

    handleChange = (value, index, field) => {
        const {keywords, updateKeyword} = this.props;
        let newKeyword = keywords[index];
        // if(field == 'hour') {
        //     newKeyword[field] = moment(value).format('MM:YY');
        // } else {
        //     newKeyword[field] = value;
        // }
        newKeyword[field] = value;
        updateKeyword(newKeyword, index);
    }

    render () {

        let {keywords, isLoading, addNewKeyword, saveKeywords} = this.props;

        const isKeywordsEmpty = () => {
            return (keywords.length == 0)
        }
        return (
            <div>
                { isLoading &&
                    <div className="full-loading">
                        <i className="fa fa-spinner fa-pulse fa-3x"></i>
                    </div>
                }
                { isKeywordsEmpty() && <Alert type="warning">Empty</Alert> }
                { !isKeywordsEmpty() &&
                    <table className="table table-striped">
                        {/* <thead>
                            <tr>
                                <th style={{width: "80px"}}></th>
                                <th>Keyword</th>
                                <th style={{width: "120px"}}>Jour</th>
                                <th style={{width: "120px"}}>Heure (hh:mm)</th>
                                <th style={{width: "120px"}}>Scroll</th>
                            </tr>
                        </thead> */}
                        <tbody>
                        {/* {keywords.map( (row, index) => (
                            <tr key={index}>
                                <td>
                                    <ToggleButton toggled={row.isActive}
                                     onToggle={(e, v) => this.handleChange(v, index, 'isActive')} />
                                </td>
                                <td>{row._id ? row.name: (<TextField disabled={!row.isActive} id={'keyword-name-'+index} value={row.name} onChange={(e,v) => this.handleChange(v, index, 'name')} />)}</td>
                                <td>
                                    <Select
                                        id={'keyword-day-'+index}
                                        autoWidth={true} 
                                        fullWidth={true} 
                                        value={row.day} 
                                        maxHeight={200}
                                        disabled={!row.isActive}
                                        onChange={ (e, i, v) => this.handleChange(v, index, 'day')}
                                        name={'keyword-day-'+index}>
                                        {daysList}
                                    >
                                    </Select>
                                </td>
                                <td>
                                    <TextField
                                        id={'keyword-scroll-'+index} 
                                        type="text"
                                        fullWidth={true} 
                                        value={row.hour || ''}
                                        disabled={!row.isActive}
                                        onChange={(e,v) => this.handleChange(v, index, 'hour')} 
                                    />
                                    <TimePicker
                                        format="24hr"
                                        hintText="24hr Format"
                                        cancelLabel="Annuler"
                                        okLabel="Valider"
                                        value={row.hour}
                                        disabled={!row.isActive}
                                        onChange={(e,v) => this.handleChange(v, index, 'hour')}
                                    />
                                </td>
                                <td>
                                    <TextField 
                                        id={'keyword-scroll-'+index} 
                                        type="number"
                                        fullWidth={true} 
                                        value={row.scroll || ''}
                                        disabled={!row.isActive}
                                        onChange={(e,v) => this.handleChange(v, index, 'scroll')} 
                                    />
                                </td>
                            </tr>
                        ))} */}
                        </tbody>
                    </table>
                }
                <div className="pt20">
                    <Button 
                        variant="contained"
                        fullWidth={true} 
                        onClick={addNewKeyword}
                    >
                        Ajouter
                    </Button>
                </div>
                <div className="pt20">
                    <Button 
                        variant="contained"
                        fullWidth={true}
                        onClick={saveKeywords}
                    >
                        Enregister
                    </Button>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(KeywordsCronJob);