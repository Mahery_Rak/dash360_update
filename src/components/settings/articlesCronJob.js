import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllArticlesJob, updateArticleJob, saveArticleJobs} from '../../actions';

import {Button, MenuItem, TextField} from '@material-ui/core';
import ToggleButton from '@material-ui/lab/ToggleButton';

import Alert from '../alert';

const hoursList = [];
for (let i = 0; i < 24; i++ ) {
    hoursList.push(<MenuItem value={i} key={i} primaryText={`${i}h`} />);
}

const mapStateToProps = (state) => {
    return {
        articleJobs: state.settings.articleCronJob.articles,
        isLoading: state.settings.articleCronJob.isLoading
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ getAllArticlesJob, updateArticleJob, saveArticleJobs }, dispatch);

class ArticlesCronJob extends Component {

    render () {

        const isEmpty = () => {
            return (articleJobs.length == 0)
        }
        let {articleJobs, isLoading, saveArticleJobs} = this.props;
        return (
            <div>
                { isLoading &&
                    <div className="full-loading">
                        <i className="fa fa-spinner fa-pulse fa-3x"></i>
                    </div>
                }
                { isEmpty() && <Alert type="warning">Empty</Alert> }
                { !isEmpty() &&
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th style={{width: "80px"}}></th>
                                <th>Article</th>
                                <th style={{width: "120px"}}>Heure (hh:mm)</th>
                            </tr>
                        </thead>
                        <tbody>
                        {articleJobs.map( (row, index) => (
                            <tr key={index}>
                                <td>
                                    <ToggleButton toggled={row.isActive} onToggle={(e, v) => this.handleChange(v, index, 'isActive')} />
                                </td>
                                <td>{row.name}</td>
                                <td>
                                    <TextField 
                                        id={'keyword-scroll-'+index} 
                                        type="text"
                                        fullWidth={true} 
                                        value={row.hour || ''}
                                        disabled={!row.isActive}
                                        onChange={(e,v) => this.handleChange(v, index, 'hour')} 
                                    />
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                }
                <div className="pt20">
                    <Button
                        fullWidth={true}
                        onClick={saveArticleJobs}
                    >
                        Enregistrer
                    </Button>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesCronJob);