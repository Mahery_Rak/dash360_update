import React, {Component} from 'react';

 class Alert extends Component {

    render() {
        const {type} = this.props;

        const style = {
            border: "1px solid",
            padding: "5px 15px",
            borderRadius: "4px",
            textAlign: "center",
            margin: "15px 0px"
        }

        if (type === 'warning') {
            style.borderColor = '#ffda8e';
            style.color = '#d8a958';
            style.backgroundColor = '#fcf8e3';
        }

        if (type === 'danger') {
            style.borderColor = '#ff8c8c';
            style.color = '#ff8c8c';
            style.backgroundColor = '#ffefef';
        }
        
        return(
            <div style={style}>
                {this.props.children}
            </div>
        )
    }
}

export default Alert;