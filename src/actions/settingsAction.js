import { SERVICE_BASE_URL, APIAxios } from '../common';

export function getAllKeywords() {
    return (dispatch) => {
        dispatch({
            type: 'SETTINGS_GET_ALL_KEYWORDS_START'
        });
        let getAllKeywords= APIAxios.get(SERVICE_BASE_URL + '/api/cronjobs', {type: 'facebook'})
        getAllKeywords.then(keywords => {
            dispatch({
                type: 'SETTINGS_GET_ALL_KEYWORDS_SUCCESS',
                keywords
            });
        });
    }
}

export function addNewKeyword() {
    return (dispatch) => {
        dispatch({
            type: 'SETTINGS_ADD_NEW_KEYWORD'
        })
    }
}

export function updateKeyword(keyword, index) {
    return (dispatch) => {
        dispatch({
            type: 'SETTINGS_UPDATE_KEYWORD',
            keyword,
            index
        })
    }
}

export function saveKeywords() {
    return (dispatch, getState) => {
        dispatch({
            type: 'SETTINGS_SAVE_KEYWORDS_START'
        })
        let keywordsToSave = [];
        const state = getState()
        
        state.settings.facebookCronJob.keywords.forEach((new_keyword) => {
            if (!new_keyword._id) {
                keywordsToSave.push(new_keyword)
            } else {
                state.settings.facebookCronJob.originKeywords.forEach((originKeyword) => {
                    if (originKeyword.name == new_keyword.name) {
                        if (originKeyword.isActive != new_keyword.isActive || 
                            originKeyword.day != new_keyword.day || 
                            originKeyword.hour != new_keyword.hour || 
                            originKeyword.scroll != new_keyword.scroll) {
                                keywordsToSave.push(new_keyword)
                        }
                    }
                });
            }
        });

        APIAxios.put(SERVICE_BASE_URL + '/api/cronjobs', keywordsToSave).then(response => {
            dispatch({
                type: 'SETTINGS_SAVE_KEYWORDS_SUCCESS'
            })
        })

    }
}

export function getKeywordsCronJobStat(parameters) {
    return (dispatch) => {
        dispatch({
            type: 'SETTINGS_GET_KEYWORDS_STAT_START'
        });
        APIAxios.get(SERVICE_BASE_URL +'/api/stat_cron_job/per_month/facebook', parameters).then(keywordsStat => {
            dispatch({
                type: 'SETTINGS_GET_KEYWORDS_STAT_SUCCESS',
                keywordsStat
            });
        });
    }
}

export function getAllArticlesJob() {
    return (dispatch) => {
        dispatch({
            type: 'SETTINGS_GET_ALL_ARTICLE_JOBS_START'
        });
        APIAxios.get(SERVICE_BASE_URL + '/api/cronjobs', {type: 'article'}).then(articles => {
            dispatch({
                type: 'SETTINGS_GET_ALL_ARTICLE_JOBS_SUCCESS',
                articles
            });
        });
    }
}

export function updateArticleJob(article, index) {
    return (dispatch) => {
        dispatch({
            type: 'SETTINGS_UPDATE_ARTICLE_JOB',
            article,
            index
        })
    }
}

export function saveArticleJobs() {
    return (dispatch, getState) => {
        dispatch({
            type: 'SETTINGS_SAVE_ARTICLE_JOBS_START'
        })
        let articleJobsToSave = [];
        const state = getState()
        
        state.settings.articleCronJob.articles.forEach((newJob) => {
            state.settings.articleCronJob.originArticleJobs.forEach((originArticleJob) => {
                if (originArticleJob.name == newJob.name) {
                    if (originArticleJob.isActive != newJob.isActive || 
                        originArticleJob.hour != newJob.hour) {
                            articleJobsToSave.push(newJob)
                    }
                }
            });
        });

        APIAxios.put(SERVICE_BASE_URL + '/api/cronjobs', articleJobsToSave).then(response => {
            dispatch({
                type: 'SETTINGS_SAVE_ARTICLE_JOBS_SUCCESS'
            })
        })
    }
}

export function getArticlesCronJobStat(parameters) {
    return (dispatch) => {
        dispatch({
            type: 'SETTINGS_GET_ARTICLES_STAT_START'
        });
        APIAxios.get(SERVICE_BASE_URL +'/api/stat_cron_job/per_month/article', parameters).then(articleJobsStat => {
            dispatch({
                type: 'SETTINGS_GET_ARTICLES_STAT_SUCCESS',
                articleJobsStat
            });
        });
    }
}
