 export const raiseServerAccessSuccess = status  => {
    return {
        type: 'RAISE_SERVER_ACCESS_SUCCESS',
        status
    }
}

 export const raiseServerAccessUnauthorized = ()  => {
    return {
        type: 'RAISE_SERVER_ACCESS_UNAUTHORIZED',
        status: 401
    }
}

 export const raiseServerAccessForbidden = ()  => {
    return {
        type: 'RAISE_SERVER_ACCESS_FORBIDDEN',
        status: 403
    }
}

 export const raiseServerAccessInternalError = ()  => {
    return {
        type: 'RAISE_SERVER_ACCESS_INTERNAL_ERROR',
        status: 500
    }
}

export const raiseServerAccessUnreachable = ()  => {
    return {
        type: 'RAISE_SERVER_ACCESS_UNREACHABLE',
        status: -1
    }
}
