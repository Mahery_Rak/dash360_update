
export function toggleSideBarAction() {
    return(dispatch) => {
        dispatch({type: 'TOGGLE_SIDEBAR'})
    }
}