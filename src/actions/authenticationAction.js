import { AUTH_BASE_URL, APIAxios  } from '../common';

export function signInAction(username, password){
    return (dispatch) => {
        let signinPromise = APIAxios.post(AUTH_BASE_URL + '/signin', {username: username, password: password})
        signinPromise.then(function (response) {
            console.log(response);
            if (response.data.token) {
                localStorage.setItem('user', response.data.token)
                dispatch({ type: 'LOG_IN_AUTH_SUCCESS' })
            } else {
                dispatch({
                    type: 'LOG_IN_AUTH_REJECTED',
                    payload: response.data.message
                })
            }
        });
    }
}

export function signOutAction() {
    return(dispatch) => {
        localStorage.removeItem('user')
        dispatch({type: 'LOG_OUT_AUTH_SUCCESS'});
        window.location.reload()
    } 
}

export function checkAuthentication() {
    return (dispatch) => {
        APIAxios.post(AUTH_BASE_URL + '/check', {})
            .then(function (response) {
                if (response.data.valid) {
                    dispatch({type: 'FETCH_AUTH_SUCCESS'})
                } else {
                    dispatch({type: 'FETCH_AUTH_REJECTED'})
                }
            });
        }
    }

